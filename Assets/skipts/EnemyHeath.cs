using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHeath : MonoBehaviour
{
    [SerializeField] private float hitPoits = 100f;

    public void TakeDamage(float damageAmount)
    {
        hitPoits -= damageAmount;
        if(hitPoits<=0)
        {
            Destroy(gameObject);
        }
    }
}
