using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUp : MonoBehaviour
{
    [SerializeField] private int amount = 10;

    private void OnTriggerEnter(Collider Other)
    {
        Ammo ammo = Other.gameObject.GetComponentInChildren<Ammo>();
        if (!ammo) return;
        ammo.AddAmmo(amount);
        Destroy(gameObject);
    }
}
