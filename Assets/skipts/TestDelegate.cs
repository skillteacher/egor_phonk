using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDelegate : MonoBehaviour
{
    delegate void WriteDelegate();
    private WriteDelegate varDelegate;

    event WriteDelegate WriteEvent;

    public bool isStartEvent = false;

    private void OnEnable()
    {
        WriteEvent += WriteToLog;
    }

    private void OnDestroy()
    {
        WriteEvent -= WriteToLog;
    }
    private void Update()
    {
        if(isStartEvent)
        {
            WriteEvent?.Invoke();
            isStartEvent = false;
        }
    }

    public void WriteToLog()
    {
        Debug.Log("Write!");
    }

    public void Write2()
    {
        Debug.Log("2");
    }
}