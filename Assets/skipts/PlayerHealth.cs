using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public event Message PlayerDeath;
   [SerializeField]

    public void TakeDamage(float damageAmount)
    {
        hitPoints -= damageAmount;
        if(hitPoints <= 0)
        {
            Debug.Log("You died!");
        }

    }

} 
