using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour

{
    [SerializeField] private float range = 100f;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksPerfab;
    [SerializeField] private float sparksLivetime = 0.1f;
    [SerializeField] private Ammo ammo;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (!ammo.IsAmmoEnough!()) return;
        ammo.ReduceAmmoByOne();
        PlayMuzzleEffect();
        Reycasting();
    }

    private void PlayMuzzleEffect()
    {

    }
    private void Reycasting()
    {

        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        HitEffect(hit.point);
        EnemyHeath enemyHalth = hit.transform.GetComponent<EnemyHeath>();
        if (enemyHalth)
        {
            enemyHalth.TakeDamage(damage);
        }
    }

    private void HitEffect(Vector3 point)
    {
        GameObject sparks = Instantiate(sparksPerfab, point, Quaternion.identity);
        Destroy(sparks, sparksLivetime);
    }
}